
def main():
    path = r'X:/framework/dccs/maya/cache_export/'  # <- path to where the tool folder was copied
    import sys
    sys.path.append(path)
    from cache_export import simple_dialog 
    reload(simple_dialog)
    ui = simple_dialog.main()
    return ui

# main()
