'''
Created on 26 nov. 2019

# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is

import sys
sys.path.append(path)


from list_demo import list_demo_app
reload(list_demo_app)  # <- to take changes that you made
ui = list_demo_app.main()  # need to store the dialog in the ui variable so it's not garbage collected

# sample pattern and key dictionary
D:\prj\{project}\assets\{type}\{asset_name}\{step}\{type}_{asset_name}_{step}_{task}_{name}_v{version}.ma

{
'project': 'ABC',
'type': 'prop',
'asset_name': 'chair',
'step': 'light',
'task': 'indoor',
'name': 'master',
'version': '001',
}

"D:\prj\ABC\assets\prop\chair\light\prop_chair_light_indoor_master_v001.ma"
@author: eduardo
'''
import sys
import os
import logging
import glob

from PySide2 import QtWidgets  # @UnresolvedImport

import loadUiType

# add lucidity
libs_path = os.path.join(os.path.dirname(__file__), 'libs')
sys.path.append(libs_path)
import lucidity  # @IgnorePep8 @UnresolvedImport


# set a logger for the script!
logger = logging.getLogger('cache_export.simple_dialog')
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'ui\\demo.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


DEFAULT_PATTERN = r'D:\prj\{project}\assets\{type}\{asset_name}\{step}\{type}_{asset_name}_{step}_{task}_{name}_v{version}.ma'
DEFAULT_DICT = '''
{
'project': 'ABC',
'type': 'prop',
'asset_name': 'chair',
'step': 'light',
'task': 'indoor',
'name': 'master',
'version': '*',
}
'''


class MainUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self, pattern=None, key_dict=None, parent=None):

        # resolve optional parameters
        if pattern is None:
            pattern = DEFAULT_PATTERN

        if key_dict is None:
            key_dict = DEFAULT_DICT

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        # set default pattern and dict
        self.pattern_lineEdit.setText(pattern)
        self.keys_textEdit.setText(key_dict)

        # connect ui buttons
        self.list_pushButton.clicked.connect(self.onListFilesClicked)
        self.selected_pushButton.clicked.connect(self.onSelectedClicked)
        self.all_pushButton.clicked.connect(self.onAllClicked)
        self.check_pushButton.clicked.connect(self.onCheckGlob)

    # template part ---

    def getPattern(self):
        ''' returns the pattern entered in the ui as str '''
        return self.pattern_lineEdit.text()

    def getTemplate(self):
        ''' returns a lucidity template based on pattern in ui
        Notes:
            On failure prompts user and returns None
        '''
        pattern = self.getPattern()

        # check we have a pattern
        if not pattern:
            message = 'Please enter a pattern!'
            logger.warning(message)
            self.errorPrompt(message)
            return

        # try to build template, if fails, prompt the user
        try:
            template = self.buildLucidityTemplate(pattern)
        except Exception, e:
            message = 'Could not build a template with pattern "{}"! {}'.format(pattern, e)
            logger.warning(message)
            self.errorPrompt(message)
            return

        return template

    def buildLucidityTemplate(self, pattern):
        ''' returns a lucidity template with our custom settings'''

        # modify the default place holder to admit : and /
        default_placeholder_expression = '[\w_.\-:/]+'
        template = lucidity.Template('temp', pattern,
                                     default_placeholder_expression=default_placeholder_expression,
                                     duplicate_placeholder_mode=lucidity.Template.STRICT,
                                     anchor=lucidity.Template.ANCHOR_BOTH)

        return template

    # keys part ---

    def isDictDataOk(self):
        ''' Returns True if a dictionary can be read from ui and False otherwise'''
        data = self.getDict()
        return data is not None

    def getDict(self):
        ''' returns a dict from the ui text field
        Notes:
            if it fails, it prompts the user and returns None
        '''

        # get text in the field
        text = self.keys_textEdit.toPlainText()

        # try to evaluate the text in the UI
        try:
            data = eval(text)
        except Exception, e:
            message = 'Could not understand the python dict in the field! {}'.format(e)
            logger.warning(message)
            self.errorPrompt(message)
            return

        # check we got a dict
        if not isinstance(data, dict):
            message = 'The field does not have a dict! {}'.format(type(data))
            logger.warning(message)
            self.errorPrompt(message)
            return

        return data

    def getKeyDict(self):
        ''' get a dict with all the keys needed by template. IF keys were not
        defined, they will have an *. This is useful for the glob
        Notes:
            if this fails, it wil prompt the user and return None
        '''

        # get a dict from ui text field
        data = self.getDict()
        if data is None:
            return

        # get template
        template = self.getTemplate()
        if not template:
            return

        # get template keys
        keys = template.keys()

        # add an * to missing keys
        for k in keys:
            if k in data:
                continue
            data[k] = '*'

        return data

    # list files ---

    def getGlobPath(self):
        ''' return a glob path builded with the template and used to list files
        Notes:
            if this fails, it wil prompt the user and return None
        '''

        # get template
        template = self.getTemplate()
        if not template:
            return

        # get keys
        key_dict = self.getKeyDict()
        if not key_dict:
            return

        try:
            glob_path = template.format(key_dict)
        except Exception as e:
            message = 'Could not format template {} with keys {} because {}'.format(template, key_dict, e)
            logger.warning(message)
            self.errorPrompt(message)
            return

        return glob_path

    def listFiles(self):
        ''' List files found using the glob
        Notes:
            if this fails, it wil prompt the user and return None
        '''

        # get glob
        glob_path = self.getGlobPath()
        if not glob_path:
            return

        # list files with glob
        glob_list = glob.glob(glob_path)

        # get template
        template = self.getTemplate()
        if not template:
            return

        # double check that each path listed with glob matches the template
        path_list = []
        for path in glob_list:
            try:
                template.parse(path)
            except Exception as e:  # @UnusedVariable
                # if we are here is because the path did not match the template
                pass

            path_list.append(path)

        return path_list

    # populate ---

    def populateList(self):
        ''' list files and adds them to the list
        Notes:
            if this fails, it wil prompt the user and return None
        '''

        # get the files
        path_list = self.listFiles()
        if path_list is None:
            return

        # clear the list (it may have previous files)
        self.listWidget.clear()

        # populate the list
        for path in path_list:

            item = QtWidgets.QListWidgetItem(path)

            # lets add the path as a member variable in the item
            item.my_path = path

            self.listWidget.addItem(item)

    # list items ---

    def getSelectedPaths(self):
        ''' Returns paths in items selected in the list '''

        # get the path list with the helper. We use selected_only option
        path_list = self._getPaths(selected_only=True)
        return path_list

    def getAllPaths(self):
        ''' Returns all paths in the list '''

        # get the path list with the helper
        path_list = self._getPaths(selected_only=False)

        return path_list

    def _getPaths(self, selected_only=False):
        ''' Helper to get the paths in the list
        Args:
            selected_only (bool): whether or not to only include selected files
        Returns:
            list[str]: a list of paths
        '''

        # get all items in list
        item_list = self.getAllItems()

        # lopp through items and get the path
        path_list = []
        for item in item_list:

            # if we are in selected only mode, check if its selected and skip if it's not
            is_item_selected = item.isSelected()
            if selected_only and not is_item_selected:
                continue

            # get the path from the item and add it to the list
            path = item.my_path
            path_list.append(path)

        return path_list

    def getAllItems(self):
        ''' Helper to get the items in the list
        Args:
            None
        Returns:
            list[QListWidgetItem]: a list of items
        '''

        # get how many items are in the list
        row_count = self.listWidget.count()

        # get the items row by row and append them to the list
        item_list = []
        for row in range(row_count):
            item = self.listWidget.item(row)
            item_list.append(item)

        return item_list

    # callbacks ---

    def onListFilesClicked(self):
        ''' callback called when clicking the list files button '''
        logger.info('onListFilesClicked')

        # check that the glob cna be build
        glob_ok = self.onCheckGlob()
        if not glob_ok:
            return

        self.populateList()

    def onSelectedClicked(self):
        ''' callback called when clicking the on selected button '''
        logger.info('onSelectedClicked')

        # get selected paths
        selected_paths = self.getSelectedPaths()

        # run our main action function on the list of paths with the helper function
        self.runOnPathList(selected_paths)

    def onAllClicked(self):
        ''' callback called when clicking the on all button '''
        logger.info('onAllClicked')

        # get ALL paths
        selected_paths = self.getAllPaths()

        # run our main action function on the list of paths with the helper function
        self.runOnPathList(selected_paths)

    def onCheckGlob(self):
        ''' callback called when clicking the check glob button
        Args:
            None
        Returns:
            bool: TRue if the glob can be built and False otherwise
        '''
        logger.info('onCheckGlob')

        # if we cant get a dict from ui, we return
        if not self.isDictDataOk():
            return False

        # get the glob from the ui, if we cant we return
        glob_path = self.getGlobPath()
        if glob_path is None:
            return False

        # update the ui with the glob just for user information
        self.glob_label.setText(glob_path)

        return True

    # replace this part! ---

    def runOnPathList(self, path_list):
        ''' Helper function to run the action function on each path in the provided list
        Args:
            path_list (list[str]): the list of paths to process
        Returns:
            None
        '''
        template = self.getTemplate()

        # we loop through the path list
        for path in path_list:

            # we have a try here so in case on path fails, the other can still run
            try:
                self.actionFunction(path, template)

            except Exception as e:
                message = 'Failed on path {} because {}'.format(path, e)
                logger.warning(message)

    def actionFunction(self, path, template):
        ''' This is the main action function, reeplace this with what you want
        to do with the path'''
        print('Doing something with')
        print(path)
        print(template.parse(path))

    # ui stuff ---

    def errorPrompt(self, message):
        title = 'Error!'
        QtWidgets.QMessageBox.warning(self, title, message)


def getMayaMainWindowAsQWidget():
    ''' Returns the maya main window as a Pyside.QWidget '''

    # get pointer to main window object in memory
    from maya import OpenMayaUI as omui  # @UnresolvedImport
    ptr = omui.MQtUtil.mainWindow()

    # wrap the object at that memory location with a QWidget
    from shiboken2 import wrapInstance  # @UnresolvedImport
    widget = wrapInstance(long(ptr), QtWidgets.QWidget)

    # return the qwidget
    return widget


def main():
    # get parent (maya window)
    parent = getMayaMainWindowAsQWidget()

    # create the ui and show it
    ui = MainUI(parent=parent)
    ui.show()

    # return it so it is not garbage collected
    return ui
